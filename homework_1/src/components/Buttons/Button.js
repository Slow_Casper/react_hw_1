import {Component} from 'react';
import "./Button.scss"

class Button extends Component {

    render() {
        const {clases, text, onClick} = this.props;
        return (
            <button 
                className={clases} 
                type='button'
                onClick={onClick}
            >
                {text}
            </button>
        )
    }
}




export default Button;