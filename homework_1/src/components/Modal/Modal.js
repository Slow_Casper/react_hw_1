import {Component} from 'react';

import "./Modal.scss"
import Button from '../Buttons/Button';


class Modal extends Component {
    render() {
        const {title, text, closeModal} = this.props
        return(
            <div 
                className='modal'
                onClick={this.close}
            >
                <div 
                    className='modal-content'
                >
                    <div className='title-container'>
                        <h2 className="modal-title">{title}</h2>
                        <button className='modal-close' onClick={closeModal}>X</button>
                    </div>
                    
                    <p className="modal-text">{text}</p>
                    <div className='btn-container'>
                        <Button text="OK"/>
                        <Button text="CANCEL" onClick={closeModal}/>
                    </div>
                </div>
            </div>
        )
    }

    close = (event) => {
        if(event.target === event.currentTarget) 
        
        this.props.closeModal()
    }
}

export default Modal;