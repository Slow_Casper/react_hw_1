import {Component} from 'react';
// import ReactDOM from 'react-dom/client';

import Button from './components/Buttons/Button';
import Modal from './components/Modal/Modal';

const modalText = {
    firstModal: {
        title: "Do you want delete this file?",
        text: "Once you delete this file, it won't be possible to undo this action. Are you sure you want to delete it?",
    },
    secondModal: {
        title: "Do something else then first button",
        text: "It mean, that second button show you another modal",
    }
}

class App extends Component {

    state = {
        isModalFirst: false,
        isModalSeckond: false
    }

    openModal(target) {
        this.setState((prev) => (prev[target] = true));
    }

    closeModal = () => {
        this.setState(() => ({         
            isModalFirst: false,
            isModalSeckond: false
        }));
    }

    render() {
        return (
            <>
                <Button  
                    clases="btn btn-green"
                    text="First button"
                    onClick={ () => {
                        this.openModal("isModalFirst")
                    }}
                />
                <Button  
                    clases="btn btn-red"
                    text="Second button"
                    onClick={ () => {
                        this.openModal("isModalSeckond")
                    }}
                />
                {
                    this.state.isModalFirst && (<Modal title={modalText.firstModal.title} text={modalText.firstModal.text} closeModal={this.closeModal}/>)
                }
                {
                    this.state.isModalSeckond && (<Modal title={modalText.secondModal.title} text={modalText.secondModal.text} closeModal={this.closeModal}/>)
                }
            </>
        )
    }


}


export default App